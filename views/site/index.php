<?php

/* @var $this yii\web\View
 * @var $searchModel   UserSearch
 * @var $dataProvider1 ActiveDataProvider
 * @var $dataProvider2 ActiveDataProvider
 * @var $filter        array
 * @var $per_page      string
 * @var $model         Departments
 */

use app\models\entity\Departments;
use app\models\search\UserSearch;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$this->title = 'Отчет';
?>
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th,
    .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 2px;
    !important;
        font-size: 12px;
    !important;
    }

    .table-responsive {
        overflow-x: visible;
    }
</style>
<div class="user-index">
    <br><br>
    <h3 class="box-title"><?= $this->title ?></h3>
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'startDate')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'endDate')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'departments')->dropDownList(Departments::getDepartmentList()) ?>
        </div>
        <div class="col-md-2" style="padding-top:25px">

            <?= Html::submitButton('Сформировать', ['class' => 'btn btn-info']); ?>
            <?= Html::a('Експорт', '#', ['id' => 'export-xlsx', 'class' => 'btn btn-info']); ?>
        </div>
    </div>


    <?php ActiveForm::end() ?>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#table1" aria-controls="table1" role="tab"
                                                  data-toggle="tab">Таблица
                1</a></li>
        <li role="presentation"><a href="#table2" aria-controls="table2" role="tab" data-toggle="tab">Таблица 2</a>
        </li>
        <li role="presentation"><a href="#table3" aria-controls="table3" role="tab" data-toggle="tab">Таблица
                3</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="table1">
            <div class="box-body">

                <?php if (isset($dataProvider1)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider1,
                        'showPageSummary' => true,
                        'columns' => [
                            ['label' => 'Время', 'attribute' => 'HourClose', 'pageSummary' => 'Всего',],
                            [
                                'label' => 'Общие <br>продажи',
                                'attribute' => 'DishDiscountSum',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>Киоск',
                                'attribute' => 'DishDiscountSumKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>драйв',
                                'attribute' => 'DishDiscountSumAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>кассы',
                                'attribute' => 'DishDiscountSumCash',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Часы',
                                'pageSummaryOptions' => ['title' => $employeesWork],
                                'attribute' => 'HourCount',
                                'value' => function ($data) {
                                    $hours = intdiv($data['HourCount'], 60);
                                    $minutes = $data['HourCount'] - $hours * 60;
                                    if ($minutes <10) {
                                        $minutes='0'.$minutes;
                                    }
                                    return $hours . ':' . $minutes;
                                },
                                'pageSummary' => function () use ($table1Summary) {
                                    $hours = intdiv($table1Summary['HourCount'], 60);
                                    $minutes = $table1Summary['HourCount'] - $hours * 60;
                                    if ($minutes <10) {
                                        $minutes='0'.$minutes;
                                    }
                                    return $hours . ':' . $minutes;

                                },
                            ],
                            [
                                'label' => 'SPMH',
                                'attribute' => 'SPMH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['SPMH']),
                            ],
                            [
                                'label' => 'Кол-во чеков<br> общее',
                                'attribute' => 'OrderCount',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $checks = '';
                                    if ($data['label']) {
                                        $array = $data['label'];
                                        ksort($array);
                                        foreach ($array as $c) {
                                            $checks.=$c;
                                        }
                                    }

                                    return '<span>' . $data['OrderCount'] .
                                        '</span><button class="check-collapse"><i class="glyphicon glyphicon-plus"></i></button><div style="display: none; width: 500px">' .
                                        $checks . '</div>';
                                },
                                'encodeLabel' => false,
                                'pageSummary' => $table1Summary['OrderCount']
                            ],
                            [
                                'label' => 'Кол-во чеков<br> по киоскам',
                                'attribute' => 'OrderCountKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков <br>драйв',
                                'attribute' => 'OrderCountAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков <br>по кассам',
                                'attribute' => 'OrderCountCash',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Средний чек',
                                'attribute' => 'OrderMiddle',
                                'encodeLabel' => false,
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['OrderMiddle']),
                            ],
                            [
                                'label' => 'Кол-во открытых<br> касс общее',
                                'attribute' => 'CashOpenCount',
                                'encodeLabel' => false,
                                'pageSummary' => true,
                                'pageSummaryFunc' => GridView::F_MAX,
                            ],
                            [
                                'label' => 'Кол-во открытых<br> касс драйв',
                                'attribute' => 'CashOpenCountAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true,
                                'pageSummaryFunc' => GridView::F_MAX,
                            ],
                            [
                                'label' => 'Кол-во открытых<br> киосков',
                                'attribute' => 'CashOpenCountKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true,
                                'pageSummaryFunc' => GridView::F_MAX,
                            ],
                            [
                                'label' => 'TPCH',
                                'attribute' => 'TPCH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['TPCH']),
                            ],
                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>
                <?php if (isset($dataProvider)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'showPageSummary' => true,
                        'columns' => [
                            ['label' => 'Время', 'attribute' => 'HourClose', 'pageSummary' => 'Всего',],
                            [
                                'label' => 'Общие <br>продажи',
                                'attribute' => 'DishDiscountSum',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>Киоск',
                                'attribute' => 'DishDiscountSumKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>автораздача',
                                'attribute' => 'DishDiscountSumAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>кассы',
                                'attribute' => 'DishDiscountSumCash',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            ['label' => 'Часы', 'attribute' => 'HourCount', 'pageSummary' => true],
                            [
                                'label' => 'SPMH',
                                'attribute' => 'SPMH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['SPMH']),
                            ],
                            [
                                'label' => 'Кол-во чеков<br> общее',
                                'attribute' => 'OrderCount',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков<br> по киоскам',
                                'attribute' => 'OrderCountKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков <br>автораздача',
                                'attribute' => 'OrderCountAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков <br>по кассам',
                                'attribute' => 'OrderCountCash',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Средний чек',
                                'attribute' => 'OrderMiddle',
                                'encodeLabel' => false,
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['OrderMiddle']),
                            ],
                            [
                                'label' => 'Кол-во открытых<br> касс общее',
                                'attribute' => 'CashOpenCount',
                                'encodeLabel' => false,
                                'pageSummary' => true,
                                'pageSummaryFunc' => GridView::F_MAX,
                            ],
                            [
                                'label' => 'Кол-во открытых<br> касс автораздача',
                                'attribute' => 'CashOpenCountAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во открытых<br> киосков',
                                'attribute' => 'CashOpenCountKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'TPCH',
                                'attribute' => 'TPCH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['TPCH']),
                            ],
                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="table2">
            <div class="box-body">

                <?php if (isset($dataProvider2)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider2,
                        'showPageSummary' => true,
                        'columns' => [
                            ['label' => 'Название', 'attribute' => 'name'],
                            [
                                'label' => 'Значение',
                                'attribute' => 'value',

                            ],
                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="table3">
            <div class="box-body">

                <?php if (isset($dataProvider3)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider3,
                        'rowOptions'=>function($data) {
                            if ($data['bold']) {
                                return ['style'=>'font-weight:bolder'];
                            }
                            return [];
                        },
                        'columns' => [
                            ['label' => 'Счет', 'attribute' => 'Account.Name'],
                            [
                                'label' => 'Сумма прихода',
                                'attribute' => 'Sum.Outgoing',
                                'format' => ['decimal', 2],
                            ],
                            [
                                'label' => 'Сумма расхода',
                                'attribute' => 'Sum.Incoming',
                                'format' => ['decimal', 2],
                            ],
                            [
                                'label' => '',
                                'value' => function ($data) {
                                    return $data['Sum.Incoming'] - $data['Sum.Outgoing'];
                                },
                                'format' => ['decimal', 2],
                            ],
                            [
                                'label' => '%',
                                'value' => function ($data) use ($table1Summary) {
                                    return (!$table1Summary['dishDiscountSum'] == 0) ? ($data['Sum.Incoming'] - $data['Sum.Outgoing']) / $table1Summary['dishDiscountSum'] * 100 : 0;
                                },
                                'format' => ['decimal', 2],
                            ],
                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


</div>
<?php
$url = \yii\helpers\Url::toRoute(['/site/export-xlsx'], true);
$script = <<<JS
$("#export-xlsx").on('click', function (){
    let startDate = $('#olap-startdate').val()
    let endDate = $('#olap-enddate').val()
    let department = $('#olap-departments').val()
    window.open("$url"+"&startDate="+startDate+"&endDate="+endDate+"&department="+department, '_blank');
    
});
$(".check-collapse").on('click', function (){
    $(this).next().toggle();
    
    
});
JS;
$this->registerJs($script);
?>

