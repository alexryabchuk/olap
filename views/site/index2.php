<?php

/* @var $this yii\web\View
 * @var $searchModel   UserSearch
 * @var $dataProvider1 ActiveDataProvider
 * @var $dataProvider2 ActiveDataProvider
 * @var $filter        array
 * @var $per_page      string
 * @var $model         Departments
 */

use app\models\entity\Departments;
use app\models\search\UserSearch;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Отчет';
$TPCH = 0;
$SPMH = 0;
$DishDiscountSumInt = 0;
$hourCount = 0;
$orderCount = 0;
$DishDiscountSumKiosk = 0;
if (isset($data)) {
    $DishDiscountSumInt = array_sum(ArrayHelper::getColumn($data['table4'], 'DishDiscountSumInt'));
    $DishDiscountSumKiosk = array_sum(ArrayHelper::getColumn($data['table4'], 'DishDiscountSumKiosk'));
    $hourCount = array_sum(ArrayHelper::getColumn($data['table4'], 'HourCount'));
    $orderCount = array_sum(ArrayHelper::getColumn($data['table4'], 'UniqOrderId'));

    $TPCH = !$hourCount == 0 ? $orderCount / $hourCount * 60 : 0;
    $SPMH = !$hourCount == 0 ? sprintf('%01.2f', $DishDiscountSumInt / $hourCount * 60) : 0;
}


?>
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th,
    .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 2px;
    !important;
        font-size: 12px;
    !important;
    }

    .table-responsive {
        overflow-x: visible;
    }
</style>
<div class="user-index">
    <br><br>
    <h3 class="box-title"><?= $this->title ?></h3>
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'startDate')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'endDate')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-md-4" style="display: none">
            <?= $form->field($model, 'departments')->dropDownList(Departments::getDepartmentList()) ?>
        </div>
        <div class="col-md-2" style="padding-top:25px">

            <?= Html::submitButton('Сформировать', ['class' => 'btn btn-info', 'id' => 'send']); ?>
            <?php //Html::a('Експорт', '#', ['id' => 'export-xlsx', 'class' => 'btn btn-info']); ?>
        </div>
    </div>


    <?php ActiveForm::end() ?>
</div>
<div id="data-table">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#table1" aria-controls="table1" role="tab" data-toggle="tab">
                Таблица 1</a></li>
        <li role="presentation"><a href="#table2" aria-controls="table2" role="tab" data-toggle="tab">Таблица 2</a></li>

        <li role="presentation"><a href="#table3" aria-controls="table3" role="tab" data-toggle="tab">Таблица 3</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="table1">
            <div class="box-body">

                <?php if (isset($dataProvider1)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider1,
                        'showPageSummary' => true,
                        'columns' => [
                            [
                                'label' => 'Предприятие',
                                'attribute' => 'DepartmentName',
                                'encodeLabel' => false,
                                'pageSummary' => 'Всего',
                            ],
                            [
                                'label' => 'Продажи <br> Всего',
                                'attribute' => 'DishDiscountSumInt',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
//                            [
//                                'label' => 'Продажи <br> ',
//                                'attribute' => 'Kiosk',
//                                'encodeLabel' => false,
//                                'pageSummary' => true
//                            ],

                            [
                                'label' => 'Продажи <br>Киоск',
                                'attribute' => 'DishDiscountSumKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Часы ',
                                'attribute' => 'HourCount',
                                'encodeLabel' => false,
                                'pageSummary' => function ($data) use ($hourCount) {
                                    $hours = intdiv($hourCount, 60);
                                    $minutes = $hourCount - $hours * 60;
                                    if ($minutes < 10) {
                                        $minutes = '0' . $minutes;
                                    }
                                    return $hours . ':' . $minutes;
                                },
                                'value' => function ($data) {
                                    $hours = intdiv($data['HourCount'], 60);
                                    $minutes = $data['HourCount'] - $hours * 60;
                                    if ($minutes < 10) {
                                        $minutes = '0' . $minutes;
                                    }
                                    return $hours . ':' . $minutes;
                                },
                            ],

                            [
                                'label' => 'SPMH',
                                'attribute' => 'SPMH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $SPMH),
                            ],
                            [
                                'label' => 'TPCH',
                                'attribute' => 'TPCH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $TPCH),
                            ],
                            [
                                'label' => 'Кол-во чеков<br> общее',
                                'attribute' => 'UniqOrderId',
                                'format' => 'raw',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Средний чек',
                                'attribute' => 'OrderMiddle',
                                'encodeLabel' => false,
                                'format' => ['decimal', 2],
                                'value' => function ($data) {
                                    if ($data['UniqOrderId'] != 0) {
                                        return sprintf("%01.2f", $data['DishDiscountSumInt'] / $data['UniqOrderId']);
                                    }
                                    return 0;

                                },
                                'pageSummary' => $orderCount != 0 ? sprintf("%01.2f", $DishDiscountSumInt / $orderCount) : 0,
                            ],


                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>
                <?php if (isset($dataProvider)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'showPageSummary' => true,
                        'columns' => [
                            ['label' => 'Время', 'attribute' => 'HourClose', 'pageSummary' => 'Всего',],
                            [
                                'label' => 'Общие <br>продажи',
                                'attribute' => 'DishDiscountSum',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>Киоск',
                                'attribute' => 'DishDiscountSumKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>автораздача',
                                'attribute' => 'DishDiscountSumAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Продажи <br>кассы',
                                'attribute' => 'DishDiscountSumCash',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            ['label' => 'Часы', 'attribute' => 'HourCount', 'pageSummary' => true],
                            [
                                'label' => 'SPMH',
                                'attribute' => 'SPMH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['SPMH']),
                            ],
                            [
                                'label' => 'Кол-во чеков<br> общее',
                                'attribute' => 'OrderCount',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков<br> по киоскам',
                                'attribute' => 'OrderCountKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков <br>автораздача',
                                'attribute' => 'OrderCountAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во чеков <br>по кассам',
                                'attribute' => 'OrderCountCash',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Средний чек',
                                'attribute' => 'OrderMiddle',
                                'encodeLabel' => false,
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['OrderMiddle']),
                            ],
                            [
                                'label' => 'Кол-во открытых<br> касс общее',
                                'attribute' => 'CashOpenCount',
                                'encodeLabel' => false,
                                'pageSummary' => true,
                                'pageSummaryFunc' => GridView::F_MAX,
                            ],
                            [
                                'label' => 'Кол-во открытых<br> касс автораздача',
                                'attribute' => 'CashOpenCountAuto',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'Кол-во открытых<br> киосков',
                                'attribute' => 'CashOpenCountKiosk',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'TPCH',
                                'attribute' => 'TPCH',
                                'format' => ['decimal', 2],
                                'pageSummary' => sprintf("%01.2f", $table1Summary['TPCH']),
                            ],
                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="table2">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">Наименование</div>
                    <div class="col-md-10">_</div>
                    <div class="col-md-2">Продажа</div>
                    <div class="col-md-10"><?= $DishDiscountSumInt ?></div>
                    <div class="col-md-2">Часы</div>
                    <div class="col-md-10"><?php
                        $hours = intdiv($hourCount, 60);
                        $minutes = $hourCount - $hours * 60;
                        if ($minutes < 10) {
                            $minutes = '0' . $minutes;
                        }
                        echo $hours . ':' . $minutes;
                        ?></div>
                    <div class="col-md-2">SPMH</div>
                    <div class="col-md-10"><?= sprintf("%01.2f", $SPMH) ?></div>
                    <div class="col-md-2">Количество чеков общее</div>
                    <div class="col-md-10"><?= $orderCount ?></div>
                    <div class="col-md-2">TPCH</div>
                    <div class="col-md-10"><?= sprintf("%01.2f", $TPCH) ?></div>
                    <div class="col-md-2">Средний чек касса</div>
                    <div class="col-md-10"><?= $orderCount != 0 ? sprintf("%01.2f", $DishDiscountSumInt / $orderCount) : 0 ?></div>
                    <div class="col-md-2">Средний чек киоск</div>
                    <div class="col-md-10"><?= $orderCount != 0 ? sprintf("%01.2f", $DishDiscountSumKiosk / $orderCount) : 0 ?></div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="table3">
            <div class="box-body">

                <?php if (isset($dataProvider2)) : ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider2,
                        'showPageSummary' => true,
                        'columns' => [
                            [
                                'label' => 'Предприятие',
                                'attribute' => 'DepartmentName',
                                'encodeLabel' => false,
                                'pageSummary' => 'Всего',
                            ],
                            [
                                'label' => 'Списание сырой продукции (итоговое) ',
                                'attribute' => 'spgot',
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],
                            [
                                'label' => '% от ТО ',
                                'attribute' => 'spgot_p',
                                'format' => ['decimal', 2],
                                'encodeLabel' => false,
                                'pageSummary' => true
                            ],

                            [
                                'label' => 'Списание готовой продукции',
                                'attribute' => 'spsir',
                                'format' => ['decimal', 2],
                                'pageSummary' => true
                            ],
                            [
                                'label' => '% от ТО',
                                'attribute' => 'spsir_p',
                                'format' => ['decimal', 2],
                                'pageSummary' => true
                            ],

                            [
                                'label' => 'OTTO ',
                                'attribute' => 'otto',
                                'format' => ['decimal', 2],
                                'pageSummary' => true
                            ],
                            [
                                'label' => '% от ТО ',
                                'attribute' => 'otto_p',
                                'format' => ['decimal', 2],
                                'pageSummary' => true
                            ],
                            [
                                'label' => 'OTTO non food',
                                'attribute' => 'otton',
                                'format' => ['decimal', 2],
                                'pageSummary' => true
                            ],
                            [
                                'label' => '% от ТО',
                                'attribute' => 'otton_p',
                                'format' => ['decimal', 2],
                                'pageSummary' => true
                            ],
                        ],
                        'panel' => [
                            'heading' => '',
                            //                        'before' => false,
                            //                        'after' => '',
                        ],
                    ]) ?>
                <?php endif; ?>

            </div>
        </div>
    </div>


</div>
<div id="loading" style="display: none; ">
    <img style="margin: 0 auto;display: block" src="loading@2x.gif">
</div>
<?php

$url = \yii\helpers\Url::toRoute(['/site/export-xlsx'], true);
$script = <<<JS
$("#export-xlsx").on('click', function (){
    let startDate = $('#olap-startdate').val()
    let endDate = $('#olap-enddate').val()
    let department = $('#olap-departments').val()
    window.open("$url"+"&startDate="+startDate+"&endDate="+endDate+"&department="+department, '_blank');
    
});
$(".check-collapse").on('click', function (){
    $(this).next().toggle();
    
    
});
$( "#send" ).on('mousedown', function (){
        $("#data-table").hide();
	    $( "#loading" ).show() ; //  плавно изменяем прозрачность элемента <span> за 1 секунду
	  });

JS;
$this->registerJs($script);
?>

