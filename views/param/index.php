<?php

/* @var $this yii\web\View
 * @var $model Param
 */

use app\models\entity\Param;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Настройки';
?>
<div class="param-index">
    <br><br><br><br>
    <div class="panel panel-default">
        <div class="panel-heading"><?=$this->title?></div>
        <div class="panel-body">
            <?php $form= ActiveForm::begin()?>
            <?=$form->field($model,'login')->textInput();?>
            <?=$form->field($model,'password')->textInput();?>
            <?=$form->field($model,'host')->textInput();?>
            <?=$form->field($model,'key')->textInput(['readOnly'=>true]);?>
            <?= Html::submitButton('Сохранить');?>
            <?php ActiveForm::end()?>
        </div>
    </div>

</div>
