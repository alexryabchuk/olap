<?php

/* @var $this yii\web\View
 * @var $searchModel ScoreSearch
 * @var $dataProvider ActiveDataProvider
 */

use app\models\entity\Score;
use app\models\search\ScoreSearch;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$this->title = 'Счета';
?>
<div class="user-index">
    <br><br>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
//                'rowOptions'=>function($model){
//                    /* @var $model ScoreSearch */
//                    if (!$model->is_deleted) {
//                        return ['style' => 'background-color:green'];
//                    }
//                },
                'columns' => [
                    [
                        'attribute' => 'code',
                    ],
                    [
                        'attribute' => 'name',
                    ],
                    [
                        'label' => 'Включать в отчет',
                        'format' => "raw",
                        'value' => function ($model) {
                            /* @var $model Score */
                            return (!$model->is_deleted) ? Html::a("<i class='glyphicon glyphicon-ok' style='color: green'></i>",
                                ['delete', 'id' => $model->id]) :
                                Html::a("<i class='glyphicon glyphicon-remove' style='color: red'></i>",
                                    ['delete', 'id' => $model->id]);
                        },
                    ]
                ],
                'panel' => [
                    'heading' => '<div>' . Html::a('Синхронизировать <i class="glyphicon glyphicon-refresh"></i>',
                            ['synchronize'],
                            ['title' => 'Добавить', 'class' => 'btn btn-info']) . '</div>',
                    'before' => false,
                    'after' => '',
                ],
            ]) ?>
        </div>
    </div>
</div>

