<?php

/* @var $this yii\web\View
 * @var $searchModel UserSearch
 * @var $dataProvider ActiveDataProvider
 * @var $filter array
 * @var $per_page string
 */

use app\models\enums\UserRole;
use app\models\enums\UserStatus;
use app\models\search\UserSearch;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$this->title = 'Торговые предприятия';
?>
<div class="user-index">
    <br><br>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions'=>function($model){
                    /* @var $model \app\models\search\DepartmentsSearch */
                    if ($model->is_deleted) {
                        return ['style' => 'background-color:red'];
                    }
                },
                'columns' => [
                    [
                        'attribute' => 'code',
                    ],
                    [
                        'attribute' => 'name',
                    ],
                    [
                        'attribute' => 'department_id',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{delete}',
                    ]
                ],
                'panel' => [
                    'heading' => '<div>' . Html::a('Синхронизировать <i class="glyphicon glyphicon-refresh"></i>', ['synchronize'],
                            ['title' => 'Добавить', 'class' => 'btn btn-info']) . '</div>',
                    'before' => false,
                    'after' => '',
                ],
            ]) ?>
        </div>
    </div>
</div>

