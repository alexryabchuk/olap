<?php

/* @var $this yii\web\View
 * @var $searchModel UserSearch
 * @var $dataProvider ActiveDataProvider
 * @var $filter array
 * @var $per_page string
 */

use app\models\enums\UserRole;
use app\models\enums\UserStatus;
use app\models\search\UserSearch;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$this->title = 'Список пользователей';
?>
<div class="user-index">
    <br><br>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'username',
                    ],
                    [
                        'attribute' => 'email',
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => UserStatus::listData(),
                        'format' => 'html',
                        'value' => function ($data) {
                            /* @var $data UserSearch */
                            return UserStatus::getLabel($data->status);
                        }
                    ],
                    [
                        'attribute' => 'role',
                        'filter' => UserRole::listData(),
                        'format' => 'html',
                        'value' => function ($data) {
                            /* @var $data UserSearch */
                            return UserRole::getLabel($data->role);
                        }
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update} {delete}',
                    ]
                ],
                'panel' => [
                    'heading' => '<div>' . Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                            ['title' => 'Добавить', 'class' => 'btn btn-info']) . '</div>',
                    'before' => false,
                    'after' => '',
                ],
            ]) ?>
        </div>
    </div>
</div>

