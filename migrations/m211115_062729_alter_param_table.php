<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%param}}`.
 */
class m211115_062729_alter_param_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%param}}', 'host',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%param}}','host');
    }
}
