<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%score}}`.
 */
class m210513_184926_create_score_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%score}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название счета'),
            'code' => $this->string()->comment('Код счета'),
            'is_deleted' => $this->boolean()->defaultValue(true)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%score}}');
    }
}
