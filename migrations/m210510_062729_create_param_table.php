<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%param}}`.
 */
class m210510_062729_create_param_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%param}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'key' => $this->string(),
        ], $tableOptions);
        $this->insert('{{%param}}', [
            'login' => 'lemma',
            'password' => '2700805'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%param}}');
    }
}
