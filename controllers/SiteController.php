<?php

namespace app\controllers;

use app\models\ApiOLAP;
use app\models\entity\Param;
use app\models\OLAP;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\base\BaseObject;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['logout', 'index'],
				'rules' => [
					[
						'actions' => ['logout', 'index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex()
	{

		$olapModel = new OLAP();

		if ($olapModel->load(Yii::$app->request->post())) {
			$data = $olapModel->getReport();
			$dataProvider1 = new ArrayDataProvider([
				'allModels' => $data['table1'],
				'pagination' => [
					'pageSize' => 100,
				],
			]);
			$dataProvider2 = new ArrayDataProvider([
				'allModels' => $data['table2'],
				'pagination' => [
					'pageSize' => 100,
				],
			]);
			$dataProvider3 = new ArrayDataProvider([
				'allModels' => $data['table3'],
				'pagination' => [
					'pageSize' => 100,
				],
			]);

			return $this->render('index',
				[
					'model' => $olapModel,
					'dataProvider1' => $dataProvider1,
					'dataProvider2' => $dataProvider2,
					'dataProvider3' => $dataProvider3,
					'table1Summary' => $data['table1Summary'],
                    'employeesWork' => $data['employeesWork']
				]);
		} else {
			return $this->render('index', ['model' => $olapModel]);
		}

	}

	public function actionIndex2()
	{

        $olapModel =new OLAP();
        if ($olapModel->load(Yii::$app->request->post())) {
            $data  = $olapModel->getReport2();
            $dataProvider1 = new ArrayDataProvider([
                'allModels' => $data['table4'],
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            $dataProvider2 = new ArrayDataProvider([
                'allModels' => $data['table5'],
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            return $this->render('index2',
                [
                    'data'=>$data,
                    'model' => $olapModel,
                    'dataProvider1' => $dataProvider1,
                    'dataProvider2' => $dataProvider2,
                ]);
        } else {
            return $this->render('index2', ['model' => $olapModel]);
        }

	}

	/**
	 * Login action.
	 *
	 * @return Response|string
	 */
	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}

		$model->password = '';
		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionExportXlsx($startDate, $endDate, $department)
	{
		$olapModel = new OLAP();
		$olapModel->startDate = date('Y-m-d', strtotime($startDate));
		$olapModel->endDate = date('Y-m-d', strtotime($endDate));;
		$olapModel->departments = $department;
		$data = $olapModel->getReport();

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
		$sheet->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.4);
		$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.4);
		$sheet->setCellValue('A2', 'Время');
		$sheet->getColumnDimension('A')->setWidth(6);
		$sheet->setCellValue('B2', 'Общие продажи');
		$sheet->getColumnDimension('B')->setWidth(7);
		$sheet->setCellValue('C2', 'Продажи Киоск');
		$sheet->getColumnDimension('C')->setWidth(7);
		$sheet->setCellValue('D2', 'Продажи автораздача');
		$sheet->getColumnDimension('D')->setWidth(7);
		$sheet->setCellValue('E2', 'Продажи кассы');
		$sheet->getColumnDimension('E')->setWidth(7);
		$sheet->setCellValue('F2', 'Часы');
		$sheet->getColumnDimension('F')->setWidth(6);
		$sheet->setCellValue('G2', 'SPMH');
		$sheet->getColumnDimension('G')->setWidth(7);
		$sheet->setCellValue('H2', 'Кол-во чеков общее');
		$sheet->getColumnDimension('H')->setWidth(7);
		$sheet->setCellValue('I2', 'Кол-во чеков по киоскам');
		$sheet->getColumnDimension('I')->setWidth(7);
		$sheet->setCellValue('J2', 'Кол-во чеков автораздача');
		$sheet->getColumnDimension('J')->setWidth(7);
		$sheet->setCellValue('K2', 'Кол-во чеков по кассам');
		$sheet->getColumnDimension('K')->setWidth(7);
		$sheet->setCellValue('L2', 'Средний чек');
		$sheet->getColumnDimension('L')->setWidth(7);
		$sheet->setCellValue('M2', 'Кол-во открытых касс общее');
		$sheet->getColumnDimension('M')->setWidth(7);
		$sheet->setCellValue('N2', 'Кол-во открытых касс автораздача');
		$sheet->getColumnDimension('N')->setWidth(7);
		$sheet->setCellValue('O2', 'Кол-во открытых киосков');
		$sheet->getColumnDimension('O')->setWidth(7);
		$sheet->setCellValue('P2', 'TPCH');
		$sheet->getColumnDimension('P')->setWidth(6);
		$sheet->getStyle('A2:P2')->getAlignment()->setWrapText(true);
		$sheet->getStyle('A2:P27')->applyFromArray([
			'font' => [
				'name' => 'Arial',
				'size' => 8
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [
						'rgb' => '808080'
					]
				],
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			]
		]);
		$sheet->getStyle('A2:P2')->applyFromArray([
			'font' => [
				'bold' => true,
			],
		]);
		foreach ($data['table1'] as $item) {
			$sheet->getRowDimension(strval(intval($item['HourClose']) + 2))->setRowHeight(10);
			$sheet->setCellValue('A' . strval(intval($item['HourClose']) + 3), $item['HourClose']);
			$sheet->setCellValue('B' . strval(intval($item['HourClose']) + 3), $item['DishDiscountSum']);
			$sheet->setCellValue('C' . strval(intval($item['HourClose']) + 3), $item['DishDiscountSumKiosk']);
			$sheet->setCellValue('D' . strval(intval($item['HourClose']) + 3), $item['DishDiscountSumAuto']);
			$sheet->setCellValue('E' . strval(intval($item['HourClose']) + 3), $item['DishDiscountSumCash']);
			$sheet->setCellValue('F' . strval(intval($item['HourClose']) + 3), $item['HourCount']/60);
			$sheet->getStyle('F' . strval(intval($item['HourClose']) + 3))->getNumberFormat()
				->setFormatCode('0.00');
			$sheet->setCellValue('G' . strval(intval($item['HourClose']) + 3), $item['SPMH']);
			$sheet->getStyle('G' . strval(intval($item['HourClose']) + 3))->getNumberFormat()
				->setFormatCode('0.00');
			$sheet->setCellValue('H' . strval(intval($item['HourClose']) + 3), $item['OrderCount']);
			$sheet->setCellValue('I' . strval(intval($item['HourClose']) + 3), $item['OrderCountKiosk']);
			$sheet->setCellValue('J' . strval(intval($item['HourClose']) + 3), $item['OrderCountAuto']);
			$sheet->setCellValue('K' . strval(intval($item['HourClose']) + 3), $item['OrderCountCash']);
			$sheet->setCellValue('L' . strval(intval($item['HourClose']) + 3), $item['OrderMiddle']);
			$sheet->getStyle('L' . strval(intval($item['HourClose']) + 3))->getNumberFormat()
				->setFormatCode('0.00');
			$sheet->setCellValue('M' . strval(intval($item['HourClose']) + 3), $item['CashOpenCount']);
			$sheet->setCellValue('O' . strval(intval($item['HourClose']) + 3), $item['CashOpenCountKiosk']);
			$sheet->setCellValue('N' . strval(intval($item['HourClose']) + 3), $item['CashOpenCountAuto']);
			$sheet->setCellValue('P' . strval(intval($item['HourClose']) + 3), $item['TPCH']);
			$sheet->getStyle('P' . strval(intval($item['HourClose']) + 3))->getNumberFormat()
				->setFormatCode('0.00');
		}
		$sheet->getStyle('G27')->getNumberFormat()
			->setFormatCode('0.00');
		$sheet->getStyle('L27')->getNumberFormat()
			->setFormatCode('0.00');
		$sheet->getStyle('P27')->getNumberFormat()
			->setFormatCode('0.00');
		$sheet->setCellValue('B27', 'Всего');
		$sheet->setCellValue('B27', '=SUM(B3:B26)');
		$sheet->setCellValue('C27', '=SUM(C3:C26)');
		$sheet->setCellValue('D27', '=SUM(D3:D26)');
		$sheet->setCellValue('E27', '=SUM(E3:E26)');
		$sheet->setCellValue('F27', '=SUM(F3:F26)');
		$sheet->setCellValue('G27', '=B27/F27');
		$sheet->setCellValue('H27', '=SUM(H3:H26)');
		$sheet->setCellValue('I27', '=SUM(I3:I26)');
		$sheet->setCellValue('J27', '=SUM(J3:J26)');
		$sheet->setCellValue('K27', '=SUM(K3:K26)');
		$sheet->setCellValue('L27', '=B27/H27');
		$sheet->setCellValue('M27', '=MAX(M3:M26)');
		$sheet->setCellValue('N27', '=MAX(N3:N26)');
		$sheet->setCellValue('O27', '=MAX(O3:O26)');
		$sheet->setCellValue('P27', '=H27/F27');
		$sheet->getStyle('A27:P27')->applyFromArray([
			'font' => [
				'bold' => true,
			],
		]);
		$i = 30;
		$sheet->mergeCells('A29:D29');
		$sheet->mergeCells('E29:F29');
		$sheet->getStyle('A28:P28')->applyFromArray([
			'font' => [
				'bold' => true,
			],
		]);
		$sheet->setCellValue('A29', 'Название');
		$sheet->setCellValue('E29', 'Значение');
		$sheet->getStyle('A29:F29')->applyFromArray([
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [
						'rgb' => '808080'
					]
				],
			],
		]);
		foreach ($data['table2'] as $item) {
			$sheet->mergeCells('A' . $i . ':D' . $i);
			$sheet->mergeCells('E' . $i . ':F' . $i);
			$sheet->setCellValue('A' . $i, $item['name']);
			$sheet->setCellValue('E' . $i, $item['value']);
			$sheet->getStyle('E' . $i)->getNumberFormat()
				->setFormatCode('0.00');
			$sheet->getStyle('A' . $i . ':F' . $i)->applyFromArray([
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '808080'
						]
					],
				],
			]);
			$i++;
		}

		$i = 30;
		$sheet->mergeCells('A1:P1');
		$sheet->setCellValue('A1', $startDate . $endDate . $department);
		$sheet->mergeCells('H29:K29');
		$sheet->mergeCells('H29:K29');
		$sheet->mergeCells('L29:M29');
		$sheet->mergeCells('N29:O29');
		$sheet->setCellValue('H29', 'Счет');
		$sheet->setCellValue('L29', 'Значение');
		$sheet->setCellValue('N29', '%');
		$sheet->getStyle('H29:O29')->applyFromArray([
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [
						'rgb' => '808080'
					]
				],
			],
		]);
		foreach ($data['table3'] as $item) {
			$sheet->mergeCells('H' . $i . ':K' . $i);
			$sheet->mergeCells('L' . $i . ':M' . $i);
			$sheet->mergeCells('N' . $i . ':O' . $i);
			$sheet->setCellValue('H' . $i, $item['Account.Name']);
			$sheet->setCellValue('L' . $i, abs($item['Sum.Incoming'] - $item['Sum.Outgoing']));
			$sheet->getStyle('L' . $i)->getNumberFormat()
				->setFormatCode('0.00');
			$sheet->setCellValue('N' . $i, '=L' . $i . '/B27*100');
			$sheet->getStyle('N' . $i)->getNumberFormat()
				->setFormatCode('0.000');
			$sheet->getStyle('H' . $i . ':O' . $i)->applyFromArray([
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '808080'
						]
					],
				],
			]);
			$i++;
		}

		$sheet->getRowDimension(2)->setRowHeight(80);
		$sheet->getRowDimension('2')->setRowHeight(80);
		$writer = new Xlsx($spreadsheet);

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=export.xlsx");
		header("Content-Transfer-Encoding: binary ");

		ob_end_clean();
		ob_start();
		$writer->save('php://output');
	}
}

