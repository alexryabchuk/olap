<?php

namespace app\controllers;

use app\models\ApiOLAP;
use app\models\entity\Score;
use app\models\search\ScoreSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ScoreController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index','delete','synchronize'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ScoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionSynchronize()
    {
        ApiOLAP::loadScore();
        return $this->redirect(['/score/index']);

    }

    public function actionDelete($id) {
        $score = Score::findOne($id);
        /* @var $score Score */
        if ($score->is_deleted) {
            $score->is_deleted = 0;
        } else {
            $score->is_deleted = 1;
        }
        $score->save();
        return $this->redirect(['/score/index']);

    }


}
