<?php

namespace app\controllers;

use app\models\ApiOLAP;
use app\models\entity\Departments;
use app\models\search\DepartmentsSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class DepartmentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index','delete','synchronize'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new DepartmentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionSynchronize()
    {
        ApiOLAP::loadDepartments();
        return $this->redirect('/departments/index');

    }

    public function actionDelete($id) {
        $dep = Departments::findOne($id);
        /* @var $dep Departments */
        if ($dep->is_deleted) {
            $dep->is_deleted = 0;
        } else {
            $dep->is_deleted = 1;
        }
        $dep->save();
        return $this->redirect(['/departments/index']);

    }


}
