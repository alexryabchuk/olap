<?php

namespace app\models\entity;

use setasign\Fpdi\PdfParser\Filter\Lzw;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "departments".
 *
 * @property int $id
 * @property string|null $department_id
 * @property string|null $code
 * @property string|null $name
 * @property int|null $is_deleted
 */
class Departments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_deleted'], 'integer'],
            [['department_id', 'code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department_id' => 'Department ID',
            'code' => 'Code',
            'name' => 'Name',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public static function getDepartmentList()
    {
        return ArrayHelper::map(self::find()->where(['is_deleted' => 0])->all(), 'name', 'name');
    }

    public static function getDepartmentByName($name)
    {
        return self::find()->where(['name'=>$name])->one();

    }
}
