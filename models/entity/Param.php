<?php

namespace app\models\entity;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "param".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string|null $key
 * @property string|null $host
 */
class Param extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'param';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password', 'key','host'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'key' => 'Key',
            'host' => 'host',

        ];
    }
}
