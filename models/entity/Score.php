<?php

namespace app\models\entity;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "score".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $code
 * @property int|null $is_deleted
 */
class Score extends ActiveRecord
{
    public static $reservedScore = ['Списание сырой продукции','Списание готовой продукции','Недостача инвентаризации','Излишки инвентаризации'];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'score';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_deleted'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название счета',
            'code' => 'Код счета',
            'is_deleted' => 'Отображать в отчете',
        ];
    }
}
