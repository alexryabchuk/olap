<?php

namespace app\models\search;

use app\models\entity\Score;
use yii\data\ActiveDataProvider;

class ScoreSearch extends Score
{

    public function rules()
    {
        return [
            [['name', 'code', 'is_deleted'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = Score::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'is_deleted' => $this->is_deleted,
        ]);

        $query
            ->andFilterWhere(['like', Score::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', Score::tableName() . '.code', $this->code]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'name' => [
                'asc' => [Score::tableName() . '.name' => SORT_ASC],
                'desc' => [Score::tableName() . '.name' => SORT_DESC],
            ],
            'code' => [
                'asc' => [Score::tableName() . '.code' => SORT_ASC],
                'desc' => [Score::tableName() . '.code' => SORT_DESC],
            ],
            'is_deleted' => [
                'asc' => [Score::tableName() . '.is_deleted' => SORT_ASC],
                'desc' => [Score::tableName() . '.is_deleted' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}