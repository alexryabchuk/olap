<?php

namespace app\models\search;

use app\models\entity\Departments;
use yii\data\ActiveDataProvider;

class DepartmentsSearch extends Departments
{

    public function rules()
    {
        return [
            [['name', 'code', 'department_id', 'is_deleted'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = Departments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'is_deleted' => $this->is_deleted,
        ]);

        $query
            ->andFilterWhere(['like', Departments::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', Departments::tableName() . '.department_id', $this->department_id])
            ->andFilterWhere(['like', Departments::tableName() . '.code', $this->code]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'name' => [
                'asc' => [Departments::tableName() . '.name' => SORT_ASC],
                'desc' => [Departments::tableName() . '.name' => SORT_DESC],
            ],
            'code' => [
                'asc' => [Departments::tableName() . '.code' => SORT_ASC],
                'desc' => [Departments::tableName() . '.code' => SORT_DESC],
            ],
            'is_deleted' => [
                'asc' => [Departments::tableName() . '.is_deleted' => SORT_ASC],
                'desc' => [Departments::tableName() . '.is_deleted' => SORT_DESC],
            ],
            'department_id' => [
                'asc' => [Departments::tableName() . '.department_id' => SORT_ASC],
                'desc' => [Departments::tableName() . '.department_id' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}