<?php

namespace app\models;

use app\models\entity\Departments;
use app\models\entity\Param;
use app\models\entity\Score;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class ApiOLAP
{
    private $startDate;
    private $endDate;
    private $departments;
    public $_key;
    public $host;
    public $employeesWork = [];
    private $departmentCode;


    public function __construct($key,$startDate, $endDate, $departments = null)
    {

        /* @var $param Param */
        $param = Param::find()->one();
        $this->host = $param->host;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->departments = $departments;
        $this->_key = $key;

        /* @var $departmentCode Departments */
        if ($departments) {
            $departmentCode = Departments::getDepartmentByName($this->departments);
            if (!$departmentCode) {
                die($departments);
            }
            $this->departmentCode = $departmentCode->code;
            $this->employeesWork = $this->getEmployees();
        }

    }


    public function getTransaction()
    {
        $requiredScores = [
            'Излишки инвентаризации',
            'Недостача инвентаризации',
            'Списание готовой продукции',
            'Списание сырой продукции',
            'Истечение сроков годности сырья',
            'Списание отходов овощей',
            'ОТТО',
            'Специализированная химия',
            'Расходные материалы',
            'Моющие и дезинфецирующие средства',
        ];
        $scoreList = [];
        $scores = Score::find()->where(['is_deleted' => 0])->all();
        /* @var $score Score */
        foreach ($scores as $score) {
            $scoreList[] = $score->name;
        }
        foreach ($requiredScores as $required) {
            if (!in_array($required, $scoreList)) {
                $scoreList[] = $required;
            }
        }
        $ch = curl_init('http://'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "TRANSACTIONS",
            "groupByColFields" => [
                "Account.Name",
                "Account.Code"
            ],
            "aggregateFields" => [
                "Sum.Incoming",
                "Sum.Outgoing"
            ],
            "filters" => [
                "DateTime.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . '+1 days')),
                ],
                "Department" => [
                    "filterType" => "IncludeValues",
                    "values" => [$this->departments]
                ],
                "Account.Name" => [
                    "filterType" => "IncludeValues",
                    "values" => $scoreList
                ],

            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        $inv1 = 0;
        $inv2 = 0;
        $got = 0;
        $sir = 0;
        $srok = 0;
        $othod = 0;
        $otto = 0;
        $nonFood = 0;

        foreach ($data['data'] as $item) {
            if ($item["Account.Name"] == 'Излишки инвентаризации') {
                $inv2 = $item['Sum.Outgoing'];
            }
            if ($item["Account.Name"] == 'Недостача инвентаризации') {
                $inv1 = $item['Sum.Incoming'];
            }
            if ($item["Account.Name"] == 'Списание готовой продукции') {
                $got = $item['Sum.Incoming'];
            }
            if ($item["Account.Name"] == 'Списание сырой продукции') {
                $sir = $item['Sum.Incoming'];
            }
            if ($item["Account.Name"] == 'Истечение сроков годности сырья') {
                $srok = $item['Sum.Incoming'];
            }
            if ($item["Account.Name"] == 'Списание отходов овощей') {
                $othod = $item['Sum.Incoming'];
            }
            if ($item["Account.Name"] == 'ОТТО') {
                $otto = $item['Sum.Incoming'];
            }
            if (in_array($item["Account.Name"], [
                'Специализированная химия',
                'Расходные материалы',
                'Моющие и дезинфецирующие средства',
            ])) {
                $nonFood += $item['Sum.Incoming'];
            }
        }
        $array = [];
        $array[] = [
            'Account.Name' => "Разница по инвентаризации",
            "Sum.Outgoing" => $inv2,
            'Sum.Incoming' => $inv1,
            'bold' => true
        ];
        $array['spgot'] = [
            'Account.Name' => "Списание готовой продукции",
            "Sum.Outgoing" => 0,
            'Sum.Incoming' => $got,
            'bold' => true
        ];
        $array['spsir'] = [
            'Account.Name' => "Списание сырой продукции (Итого)",
            "Sum.Outgoing" => 0,
            'Sum.Incoming' => $sir + $srok + $othod,
            'bold' => true
        ];
        $array[] = [
            'Account.Name' => "Списание сырой продукции",
            "Sum.Outgoing" => 0,
            'Sum.Incoming' => $sir,
            'bold' => false
        ];
        $array[] = [
            'Account.Name' => "Истечение сроков годности сырья",
            "Sum.Outgoing" => 0,
            'Sum.Incoming' => $srok,
            'bold' => false
        ];
        $array[] = [
            'Account.Name' => "Списание отходов овощей",
            "Sum.Outgoing" => 0,
            'Sum.Incoming' => $othod,
            'bold' => false
        ];
        $array['otto'] = ['Account.Name' => "ОТТО", "Sum.Outgoing" => 0, 'Sum.Incoming' => $otto, 'bold' => true];
        $array['otton'] = ['Account.Name' => "ОТТО non-food", "Sum.Outgoing" => 0, 'Sum.Incoming' => $nonFood, 'bold' => true];

        return $array;


    }

    public static function loadDepartments()
    {
        $param = Param::find()->one();
        $host = $param->host;
        $olap = new OLAP();
        var_dump($olap->loginOLAP());
        die();

        $ch = curl_init('http://'.$host.'/resto/api/corporation/departments?key=' . $olap->loginOLAP());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $xmlString = curl_exec($ch);
        var_dump($xmlString);
        die();
        $xml = simplexml_load_string($xmlString, 'SimpleXMLElement', LIBXML_NOCDATA);
        curl_close($ch);
        Departments::deleteAll();
        foreach ($xml->corporateItemDto as $el) {
            $dep = new Departments();
            $dep->code = (string)$el->code;
            $dep->name = (string)$el->name;
            $dep->department_id = (string)$el->id;
            $dep->save();
        }
        $olap->logoutOLAP();
    }

    public static function loadScore($key)
    {
        $param = Param::find()->one();
        $host = $param->host;
        $ch = curl_init('http://'.$host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "TRANSACTIONS",
            "buildSummary" => "false",
            "groupByColFields" => [
                "Account.Name",
                "Account.Code"
            ],
            "filters" => [
                "DateTime.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', time() - 60 * 60 * 24 * 60),
                    "to" => date('Y-m-d'),
                ],
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        Score::deleteAll();
        foreach ($data['data'] as $score) {
            $dep = new Score();
            $dep->code = (string)$score["Account.Code"];
            $dep->name = (string)$score["Account.Name"];
            $dep->save();
        }
    }

    public function getSales()
    {
        $ch = curl_init('http://'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "SALES",
            "buildSummary" => "false",
            "groupByColFields" => [
                "HourClose",
                "PayTypes",
                "CashRegisterName"
            ],
            "aggregateFields" => [
                "DishDiscountSumInt",
            ],
            "filters" => [
                "OpenDate.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . ' +1 day')),
                ],
                "Department" => [
                    "filterType" => "IncludeValues",
                    "values" => [$this->departments]
                ]
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        return $data;
    }

    public function getEmployees()
    {
        /* @var $departmentCode Departments */
        $ch = curl_init('http://'.$this->host.'/resto/api/employees/byDepartment/' . $this->departmentCode . '?from='
            . date('Y-m-d', strtotime($this->startDate)) . '&to='
            . date('Y-m-d', strtotime($this->endDate))
            . '&key=' . $this->_key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $xmlString = curl_exec($ch);
        \Yii::error($this->departmentCode);
        curl_close($ch);
        $xml = simplexml_load_string($xmlString, 'SimpleXMLElement', LIBXML_NOCDATA);
        $employees = [];

        if (is_array($xml)) {
            foreach ($xml as $item) {
                $employees[(string)$item->id] = (string)$item->name;
            }
        }

        return $employees;
    }

    public function getAttendance()
    {
        $ch = curl_init('http://'.$this->host.'/resto/api/employees/attendance/byDepartment/' . $this->departmentCode . '?from='
            . date('Y-m-d', strtotime($this->startDate)) . '&to='
            . date('Y-m-d', strtotime($this->endDate))
            . '&key=' . $this->_key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($xmlString, 'SimpleXMLElement', LIBXML_NOCDATA);
        return $xml;
    }

    public function getCheck()
    {
        $ch = curl_init('http://'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "SALES",
            "buildSummary" => "false",
            "groupByColFields" => [
                "CashRegisterName",
                "PayTypes",
                "OrderNum",
                "PrechequeTime",
                "HourClose"
            ],
            "aggregateFields" => [
                "DishDiscountSumInt",

            ],
            "filters" => [
                "OpenDate.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . ' +1 day')),
                ],
                "Department" => [
                    "filterType" => "IncludeValues",
                    "values" => [$this->departments]
                ]
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        return $data;
    }

    public function getSalesByPayTypes()
    {
        $ch = curl_init('http://'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "SALES",
            "buildSummary" => "false",
            "groupByColFields" => [
                "PayTypes",
                "HourClose",
            ],
            "aggregateFields" => [
                "UniqOrderId",
            ],
            "filters" => [
                "OpenDate.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . " + 1 days")),
                ],
                "Department" => [
                    "filterType" => "IncludeValues",
                    "values" => [$this->departments]
                ]
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        return $data;
    }

    public function getSalesByCashName()
    {
        $ch = curl_init('http://'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "SALES",
            "buildSummary" => "false",
            "groupByColFields" => [
                "CashRegisterName",
                "HourClose",
            ],
            "aggregateFields" => [
                "UniqOrderId",
            ],
            "filters" => [
                "OpenDate.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . ' +1 day')),
                ],
                "Department" => [
                    "filterType" => "IncludeValues",
                    "values" => [$this->departments]
                ]
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        return $data;
    }

    public function getSalesByDepartament()
    {
        $ch = curl_init('http://9'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "SALES",
            "buildSummary" => "false",
            "groupByColFields" => [
                "Department",
                "PayTypes",

            ],
            "aggregateFields" => [
                "DishDiscountSumInt",
                "UniqOrderId",
            ],
            "filters" => [
                "OpenDate.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . " + 1 days")),
                ],
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        return $data;
    }

    public function getSalesByDepartamentCheck()
    {
        $ch = curl_init('http://'.$this->host.'/resto/api/v2/reports/olap');
        $postData = [
            "reportType" => "SALES",
            "buildSummary" => "false",
            "groupByColFields" => [
                "Department",
            ],
            "aggregateFields" => [
                "UniqOrderId",
            ],
            "filters" => [
                "OpenDate.Typed" => [
                    "filterType" => "DateRange",
                    "periodType" => "CUSTOM",
                    "from" => date('Y-m-d', strtotime($this->startDate)),
                    "to" => date('Y-m-d', strtotime($this->endDate . " + 1 days")),
                ],
            ]
        ];
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Cookie: key=" . $this->_key
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $xmlString = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($xmlString, true);
        return $data;
    }

}
