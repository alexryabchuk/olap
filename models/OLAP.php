<?php

namespace app\models;

use app\models\entity\Departments;
use app\models\entity\Param;
use app\models\entity\Score;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use function Sodium\crypto_sign_ed25519_pk_to_curve25519;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class OLAP extends Model
{
    private $apiOLAP;
    public $startDate;
    public $endDate;
    public $departments;
    private $_key;
    private $table1 = [];
    private $reportTable1 = [];
    private $table1Summary = [];
    private $table2 = [];
    private $table3 = [];
    private $table4 = [];
    private $table5 = [];
    private $employeesWork = [];
    private $table1Row = [
        'HourClose' => '',
        'DishDiscountSum' => 0,
        'DishDiscountSumKiosk' => 0,
        'DishDiscountSumAuto' => 0,
        'DishDiscountSumCash' => 0,
        'HourCount' => 0,
        'SPMH' => 0,
        'OrderCount' => 0,
        'OrderCountKiosk' => 0,
        'OrderCountAuto' => 0,
        'OrderCountCash' => 0,
        'OrderMiddle' => 0,
        'CashOpenCount' => 0,
        'CashOpenCountKiosk' => 0,
        'CashOpenCountAuto' => 0,
        'TPCH' => 0,
        'label' => '',
    ];


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['startDate', 'endDate', 'departments'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'startDate' => 'Начальная дата',
            'endDate' => 'Конечная дата',
            'departments' => 'Торговое предприятие',
        ];
    }

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->endDate = date('Y-m-d');
        $this->startDate = date('Y-m-d', strtotime(date('Y-m-d') . " - 10 days"));
        $this->loginOLAP();
    }

    public function getTable2()
    {
        $dishDiscountSum = array_sum(ArrayHelper::getColumn($this->table1, 'DishDiscountSum'));
        $this->table2[] = ['name' => 'Общие продажи', 'value' => $dishDiscountSum];
        $this->table2[] = [
            'name' => 'Продажи Киоск',
            'value' => array_sum(ArrayHelper::getColumn($this->table1, 'DishDiscountSumKiosk'))
        ];
        $hourCount = array_sum(ArrayHelper::getColumn($this->table1, 'HourCount'));
        $hours = intdiv($hourCount, 60);
        $minutes = $hourCount - $hours * 60;
        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }
        $this->table2[] = ['name' => 'Количество часов', 'value' => $hours . ':' . $minutes];
        $orderCount = array_sum(ArrayHelper::getColumn($this->table1, 'OrderCount'));
        $this->table2[] = ['name' => 'Количество чеков общее', 'value' => $orderCount];
        $this->table2[] = ['name' => 'SPMH', 'value' => !$hourCount == 0 ? sprintf('%01.2f', $dishDiscountSum / $hourCount * 60) : 0];
        $this->table2[] = ['name' => 'TPCH', 'value' => !$hourCount == 0 ? sprintf('%01.2f', $orderCount / $hourCount * 60) : 0];
        $this->table2[] = [
            'name' => 'Количество касс общее',
            'value' => is_array(ArrayHelper::getColumn($this->table1,
                'CashOpenCount')) && !empty(ArrayHelper::getColumn($this->table1,
                'CashOpenCount')) ? max(ArrayHelper::getColumn($this->table1, 'CashOpenCount')) : 0
        ];
        $dishDiscountSumCash = array_sum(ArrayHelper::getColumn($this->table1, 'DishDiscountSumCash'));
        $orderCountCash = array_sum(ArrayHelper::getColumn($this->table1, 'OrderCountCash'));
        $this->table2[] = [
            'name' => 'Средний чек касса',
            'value' => !$orderCountCash == 0 ? sprintf('%01.2f', $dishDiscountSumCash / $orderCountCash) : 0
        ];
        $dishDiscountSumKiosk = array_sum(ArrayHelper::getColumn($this->table1, 'DishDiscountSumKiosk'));
//        '' => 0,
        $orderCountKiosk = array_sum(ArrayHelper::getColumn($this->table1, 'OrderCountKiosk'));
        $this->table2[] = [
            'name' => 'Средний чек киоск',
            'value' => !$orderCountKiosk == 0 ? sprintf('%01.2f', $dishDiscountSumKiosk / $orderCountKiosk) : 0
        ];
        $dishDiscountSumAuto = array_sum(ArrayHelper::getColumn($this->table1, 'DishDiscountSumAuto'));
        $orderCountAuto = array_sum(ArrayHelper::getColumn($this->table1, 'OrderCountAuto'));
        $this->table2[] = [
            'name' => 'Средний чек автораздачи',
            'value' => !$orderCountAuto == 0 ? sprintf('%01.2f', $dishDiscountSumAuto / $orderCountAuto) : 0
        ];
        $this->table1Summary['dishDiscountSum'] = $dishDiscountSum;
        $this->table1Summary['SPMH'] = !$hourCount == 0 ? sprintf('%01.2f', $dishDiscountSum / $hourCount * 60) : 0;
        $this->table1Summary['TPCH'] = !$hourCount == 0 ? $orderCount / $hourCount * 60 : 0;
        $this->table1Summary['OrderMiddle'] = !$orderCount == 0 ? $dishDiscountSum / $orderCount : 0;
        $this->table1Summary['DishDiscountSumKiosk'] = array_sum(ArrayHelper::getColumn($this->table1, 'DishDiscountSumKiosk'));
    }

    public function getTable3()
    {
        return $this->apiOLAP->getTransaction();
    }

    private function isAuto($name)
    {
        $pos1 = mb_stripos($name, 'авто');
        $pos2 = mb_stripos($name, 'драйв');
        if ($pos1 === false and $pos2 === false) {
            return false;
        }
        return true;
    }

    public function getReport2()
    {
        set_time_limit(200);
        $department = Departments::getDepartmentList();
        $i = 0;
        foreach ($department as $item) {
            $this->departments = $item;
            $data = $this->getReport();
            $this->table4[$item] = [
                'DepartmentName' => $item,
                'DishDiscountSumInt' => $data['table1Summary']['dishDiscountSum'],
                'DishDiscountSumKiosk' => $data['table1Summary']['DishDiscountSumKiosk'],
                'SPMH' => $data['table1Summary']['SPMH'],
                'UniqOrderId' => $data['table1Summary']['OrderCount'],
                'TPCH' => $data['table1Summary']['TPCH'],
                'HourCount' => $data['table1Summary']['HourCount'],
                'OrderMiddle' => $data['table1Summary']['HourCount'],
            ];
            $d=$data['table1Summary']['dishDiscountSum'];
            $this->table5[$item] = [
                'DepartmentName' => $item,
                'spgot' => $data['table3']['spgot']["Sum.Incoming"],
                'spsir' => $data['table3']['spsir']['Sum.Incoming'],
                'otto' => $data['table3']['otto']["Sum.Incoming"],
                'otton' => $data['table3']['otton']["Sum.Incoming"],
                'spgot_p' => $d ? $data['table3']['spgot']["Sum.Incoming"]/$d*100 :0,
                'spsir_p' => $d ? $data['table3']['spsir']['Sum.Incoming']/$d*100 :0,
                'otto_p' => $d ? $data['table3']['otto']["Sum.Incoming"]/$d*100 :0,
                'otton_p' => $d ? $data['table3']['otton']["Sum.Incoming"]/$d*100 :0,
            ];
//            $i++;
//            if ($i == 5) {
//                break;
//            }
        }
//        echo "<pre>";
//        var_dump($this->table5);
//        die();

        $this->logoutOLAP();
        return [
            'table4' => $this->table4,
            'table5' => $this->table5,
        ];
    }

    public function getReport()
    {
        $this->loginOLAP();
        for ($i = 0; $i <= 23; $i++) {
            $this->table1[(string)sprintf("%'.02d", $i)] = $this->table1Row;
            $this->table1[(string)sprintf("%'.02d", $i)]['HourClose'] = (string)sprintf("%'.02d", $i);
        }
        $this->apiOLAP = new ApiOLAP($this->_key, $this->startDate, $this->endDate, $this->departments);
        $this->getSumReport();
        $this->getCheckReport();
        $this->getAttendanceReport();
        $this->getCashCount();
        $this->table1Summary['HourCount'] = 0;
        $this->table1Summary['OrderCount'] = 0;
        foreach ($this->table1 as $id => $row) {
            $this->table1[$id]['SPMH'] = ($this->table1[$id]['HourCount'] == 0) ? 0 : $this->table1[$id]['DishDiscountSum'] / $this->table1[$id]['HourCount'] * 60;
            if ($this->table1[$id]['OrderCount'] > 0) {
                $this->table1[$id]['OrderMiddle'] = $this->table1[$id]['DishDiscountSum'] / $this->table1[$id]['OrderCount'];
            }
            if ($this->table1[$id]['HourCount'] > 0) {
                $this->table1[$id]['TPCH'] = $this->table1[$id]['OrderCount'] / $this->table1[$id]['HourCount'] * 60;
            }
            $this->table1Summary['HourCount'] += $this->table1[$id]['HourCount'];
            $this->table1Summary['OrderCount'] += $this->table1[$id]['OrderCount'];
        }
        $this->getTable2();
        $this->table3 = $this->getTable3();
        ArrayHelper::multisort($this->table1, ['HourClose'], [SORT_ASC]);
        $employeesWork = '';
        foreach ($this->employeesWork as $item) {
            $employeesWork .= "Сотрудник: " . $item['name'] . " Время: " . $item['period'] . ' с ' . $item['dateFrom'] . ' до ' . $item['dateTo'] . PHP_EOL;
        }
        $this->logoutOLAP();
        return [
            'table1' => $this->table1,
            'table1Summary' => $this->table1Summary,
            'table2' => $this->table2,
            'table3' => $this->table3,
            'employeesWork' => $employeesWork
        ];
    }

    public function getReportByDepartament()
    {
        $iOLAP = new ApiOLAP('21.10.2021', '21.10.2021', '21.10.2021');
        $data = $iOLAP->getSalesByDepartament();
        if (!$data) {
            die($iOLAP->_key);
        }
        foreach ($data['data'] as $item) {
            if ($item['PayTypes'] == "(без оплаты)") {
                continue;
            }

            if (!isset($this->table4[$item['Department']])) {
                $this->table4[$item['Department']]['DishDiscountSumInt'] = $item['DishDiscountSumInt'];
                $this->table4[$item['Department']]['DepartmentName'] = $item['Department'];
                $this->table4[$item['Department']]['Kiosk'] = 0;
                $this->table4[$item['Department']]['UniqOrderId'] = 0;
                if ($item['PayTypes'] == 'КИОСК') {
                    $this->table4[$item['Department']]['Kiosk'] += $item['DishDiscountSumInt'];
                }

                if ($item['DishDiscountSumInt'] > 0) {
                    $this->table4[$item['Department']]['UniqOrderId'] = $item['UniqOrderId'];
                }
            } else {
                $this->table4[$item['Department']]['DishDiscountSumInt'] += $item['DishDiscountSumInt'];
                if ($item['PayTypes'] == 'КИОСК') {
                    $this->table4[$item['Department']]['Kiosk'] += $item['DishDiscountSumInt'];

                }

                if ($item['DishDiscountSumInt'] > 0) {
                    $this->table4[$item['Department']]['UniqOrderId'] += $item['UniqOrderId'];
                }
            }
        }
        foreach ($this->table4 as $key => $item) {
            $this->table4[$key]['Kiosk1'] = $item['Kiosk'] / $item['DishDiscountSumInt'] * 100;
        }
        // return $data;
        return $this->table4;
    }

    private function getSumReport()
    {
        $data = $this->apiOLAP->getSales();
        if (is_array($data)) {
            foreach ($data['data'] as $item) {
                $this->table1[$item['HourClose']]['DishDiscountSum'] += $item['DishDiscountSumInt'];
                if ($item['PayTypes'] == 'КИОСК') {
                    $this->table1[$item['HourClose']]['DishDiscountSumKiosk'] += $item['DishDiscountSumInt'];
                } elseif ($this->isAuto($item['CashRegisterName'])) {
                    $this->table1[$item['HourClose']]['DishDiscountSumAuto'] += $item['DishDiscountSumInt'];
                } else {
                    $this->table1[$item['HourClose']]['DishDiscountSumCash'] += $item['DishDiscountSumInt'];
                }
            }
        }
    }

    private function getAttendanceReport()
    {
        $employees = $this->apiOLAP->employeesWork;
        $xml = $this->apiOLAP->getAttendance();
        $data = [];
        $dataHours = [];
        $d = 0;
        foreach ($xml as $item) {
            $dataItem['dateFrom'] = (string)$item->dateFrom;
            $dataItem['dateTo'] = (string)$item->dateTo;

            $dateFrom = substr((string)$item->dateFrom, 0, 19);
            if (strtotime($dateFrom) < strtotime($this->startDate)) {
                $dateFrom = $this->startDate . " 00:00:00";
            }

            if (isset($item->dateTo)) {
                $dateTo = substr((string)$item->dateTo, 0, 19);
            } else {
                continue;
            };
            $dataItem['H1'] = (int)date('H', strtotime($dateFrom));
            $dataItem['M1'] = (int)date('i', strtotime($dateFrom));
            $dataItem['H2'] = (int)date('H', strtotime($dateTo));
            $dataItem['M2'] = (int)date('i', strtotime($dateTo));
            $this->employeesWork[(string)$item->employeeId] = [
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'name' => isset($employees[(string)$item->employeeId]) ? $employees[(string)$item->employeeId] : (string)$item->employeeId,
                'period' => date('H:i:s', abs(strtotime($dateTo) - strtotime($dateFrom)))
            ];
            if ((int)$dataItem['H2'] < (int)$dataItem['H1']) {
                $dataItem['H2'] = (int)$dataItem['H2'] + 24;
            }
            $data[] = $dataItem;
//            $to_time = strtotime($dataItem['dateFrom']);
//            $from_time = strtotime($dataItem['dateTo']);
//            $data[] = [
//                'dep'=>(string)$item->departmentName,
//                'dateFrom'=>$dataItem['dateFrom'],
//            'dateTo'=>$dataItem['dateTo'],
//                'min'=>round(abs($to_time - $from_time) / 60,2). " minute",
//            ];
//            $d+=round(abs($to_time - $from_time) / 60,2);
        }
//        echo $d/60;
//        echo "<pre>";
//        var_dump($data);
//        die();

        foreach ($data as $item) {
            for ($i = $item['H1']; $i <= $item['H2']; $i++) {
                if ($i == 0) {
                    continue;
                }
                if (!isset($dataHours[(string)sprintf("%'.02d", ($i > 24) ? $i - 24 : $i)])) {
                    $dataHours[(string)sprintf("%'.02d", ($i > 24) ? $i - 24 : $i)] = 0;
                }
                if ($item['H1'] == $item['H2']) {
                    $dataHours[(string)sprintf("%'.02d", ($i > 24) ? $i - 24 : $i)] += $item['M2'] - $item['M1'];
                } elseif ($i == $item['H1']) {
                    $dataHours[(string)sprintf("%'.02d", ($i > 24) ? $i - 24 : $i)] += 60 - $item['M1'];
                } elseif ($i == $item['H2']) {
                    $dataHours[(string)sprintf("%'.02d", ($i > 24) ? $i - 24 : $i)] += $item['M2'];
                } else {
                    $dataHours[(string)sprintf("%'.02d", ($i > 24) ? $i - 24 : $i)] += 60;
                }
            }
        }
        foreach ($dataHours as $id => $item) {
            if (!isset($this->table1[$id])) {
                $this->table1[$id] = $this->table1Row;
                $this->table1[$id]['HourClose'] = $id;
            }
            $this->table1[$id]['HourCount'] = $item;
        }
    }

    private function getCheckReport()
    {
        $data = $this->apiOLAP->getCheck();
//		echo "<pre>";
//		var_dump($data);
//		die();
        $dataByCheckIndex = [];
        $table1Row = [
            'HourClose' => '',
            'OrderCount' => [],
            'OrderCountKiosk' => [],
            'OrderCountAuto' => [],
            'OrderCountCash' => [],
            'label' => ''
        ];
        if (is_array($data)) {
            foreach ($data['data'] as $item) {
                if ($item['PayTypes'] == "(без оплаты)") {
                    continue;
                }
                if (!isset($dataByCheckIndex[$item['HourClose']])) {
                    $dataByCheckIndex[$item['HourClose']] = $table1Row;
                    $dataByCheckIndex[$item['HourClose']]['HourClose'] = $item['HourClose'];
                    $dataByCheckIndex[$item['HourClose']]['OrderCount'] = [];
                    $dataByCheckIndex[$item['HourClose']]['label'] = [];
                }

                if (!isset($dataByCheckIndex[$item['HourClose']]['label'][(int)$item['OrderNum']])) {
                    $dataByCheckIndex[$item['HourClose']]['label'][(int)$item['OrderNum']] = '';
                }
                $date = date('d.m.Y', strtotime($item['PrechequeTime']));
                $dataByCheckIndex[$item['HourClose']]['label'][(int)$item['OrderNum']] .= 'Каса: ' . $item["CashRegisterName"] . ' №: ' . $item['OrderNum'] .
                    ' Сума: ' . $item['DishDiscountSumInt'] . ' Дата:' . $item['PrechequeTime'] . ' Тип:' . $item['PayTypes'] . $date . '<br>';

                $dataByCheckIndex[$item['HourClose']]['OrderCount'][$item["CashRegisterName"] . $item['OrderNum'] . $date] = 1;
                $dataByCheckIndex[$item['HourClose']]['item'][] = $item;

                if ($item['PayTypes'] == 'КИОСК') {
                    if (isset($dataByCheckIndex[$item['HourClose']]['OrderCountKiosk'][$item['OrderNum']])) {
                        $dataByCheckIndex[$item['HourClose']]['OrderCountKiosk'][$item["CashRegisterName"] . $item['OrderNum'] . $date] += 1;
                    } else {
                        $dataByCheckIndex[$item['HourClose']]['OrderCountKiosk'][$item["CashRegisterName"] . $item['OrderNum'] . $date] = 1;
                    }
                } elseif ($this->isAuto($item['CashRegisterName'])) {
                    if (isset($dataByCheckIndex[$item['HourClose']]['OrderCountAuto'][$item["CashRegisterName"] . $item['OrderNum'] . $date])) {
                        $dataByCheckIndex[$item['HourClose']]['OrderCountAuto'][$item["CashRegisterName"] . $item['OrderNum'] . $date] += 1;
                    } else {
                        $dataByCheckIndex[$item['HourClose']]['OrderCountAuto'][$item["CashRegisterName"] . $item['OrderNum'] . $date] = 1;
                    }
                } else {
                    if (isset($dataByCheckIndex[$item['HourClose']]['OrderCountCash'][$item['OrderNum']])) {
                        $dataByCheckIndex[$item['HourClose']]['OrderCountCash'][$item["CashRegisterName"] . $item['OrderNum'] . $date] += 1;
                    } else {
                        $dataByCheckIndex[$item['HourClose']]['OrderCountCash'][$item["CashRegisterName"] . $item['OrderNum'] . $date] = 1;
                    }

                }
            }
//			echo "<pre>";
//			$a = $dataByCheckIndex[16];
//			ksort($a);
//			var_dump($a);
//			die();
            foreach ($dataByCheckIndex as $item) {
                if (!isset($this->table1[$item['HourClose']])) {
                    $this->table1[$item['HourClose']] = $this->table1Row;
                    $this->table1[$item['HourClose']]['HourClose'] = $item['HourClose'];
                }
                $this->table1[$item['HourClose']]['label'] = $dataByCheckIndex[$item['HourClose']]['label'];
                $this->table1[$item['HourClose']]['OrderCount'] = count($dataByCheckIndex[$item['HourClose']]['OrderCount']);
                $this->table1[$item['HourClose']]['OrderCountKiosk'] = count($dataByCheckIndex[$item['HourClose']]['OrderCountKiosk']);
                $this->table1[$item['HourClose']]['OrderCountAuto'] = count($dataByCheckIndex[$item['HourClose']]['OrderCountAuto']);
                $this->table1[$item['HourClose']]['OrderCountCash'] = count($dataByCheckIndex[$item['HourClose']]['OrderCountCash']);
            }
        }

    }

    private function getCashCount()
    {
        $data = $this->apiOLAP->getSalesByPayTypes();
        if (is_array($data)) {
            foreach ($data['data'] as $item) {
                if ($item['UniqOrderId'] > 0) {
                    if ($item['PayTypes'] == 'КИОСК') {
                        $this->table1[$item['HourClose']]['CashOpenCountKiosk'] += 1;
                    }
                }
            }
        }

        $data = $this->apiOLAP->getSalesByCashName();
        if (is_array($data)) {
            foreach ($data['data'] as $item) {
                if ($item['UniqOrderId'] > 0) {
                    $this->table1[$item['HourClose']]['CashOpenCount'] += 1;
                    if ($this->isAuto($item['CashRegisterName'])) {
                        $this->table1[$item['HourClose']]['CashOpenCountAuto'] += 1;
                    }
                }
            }
        }
        foreach ($this->table1 as $item) {
            $this->table1[$item['HourClose']]['CashOpenCount'] += $this->table1[$item['HourClose']]['CashOpenCountKiosk'];
        }


    }

    public function loginOLAP()
    {

        //86.57.218.149:65090
        //dev_roman
        //85b9f518eee6e2395f021d7f34fc74d4a2bb825d

        //95.161.174.42:9081
        //lemma
        //b40215b75884d8f78ccac7941c6b7cf2f6673885

        /* @var $param Param */
        $param = Param::find()->one();
        $ch = curl_init('http://' . $param->host . '/resto/api/auth?login=' . $param->login . '&pass=' . $param->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $this->_key = curl_exec($ch);
        $param->key = $this->_key;
        $param->save();
        curl_close($ch);
        return $this->_key;
    }

    public function logoutOLAP()
    {
        //http://host:port/resto/api/logout?key=[token]
        /* @var $param Param */
        $param = Param::find()->one();
        $ch = curl_init('http://' . $param->host . '/resto/api/logout?key=' . $this->_key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $this->_key = curl_exec($ch);
        curl_close($ch);
        return $this->_key;
    }
}
